Version too high:
 - aeson
 - optparse-applicative in port is 0.14.0.0 while stack needs == 0.13.*

Missing:
 - binary-tagged >=0.1.1
 - cryptonite-conduit >=0.1 && <0.3
 - echo >=0.1.3 && <0.2
 - filelock >=0.1.0.1
 - hpack >=0.17.0 && <0.19
 - http-client >=0.5.3.3
 - http-client-tls >=0.3.4
 - microlens-mtl >=0.1.10.0
 - mintty >=0.1.1
 - open-browser >=0.2.1
 - path >=0.5.8 && <0.6
 - path-io >=1.1.0 && <2.0.0
 - pid1 ==0.1.*
 - regex-applicative-text >=0.1.0.1 && <0.2
 - retry >=0.6 && <0.8
 - store >=0.4.1 && <0.5
 - store-core ==0.4.*
 - text-binary -any
 - text-metrics >=0.1 && <0.4
 - unicode-transforms >=0.1 && <0.4
 - vector-binary-instances -any
 - zip-archive >=0.2.3.7 && <0.4

Needs update:
 - http-conduit >=2.2.3
